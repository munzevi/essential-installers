# zsh install 
echo '#####################################################'
echo 'install timeshift'
echo '#####################################################'
sudo apt install zsh
echo 'alias ll="ls -lah" \nalias publicIp="curl ifconfig.me" \nalias please="sudo" \nalias ifcon="curl ifconfig.me" \n\nalias pa="php artisan" \nalias pas="php artisan serve"\nalias pam="php artisan migrate"\nalias pac="php artisan make:controller"\nalias pat="php artisan tinker"\nalias pam:m="php artisan make:migrate"\nalias pad:s="php artisan db:seed"\nalias pam:r="php artisan migrate:refresh"\nalias par:l="php artisan route:list"\n\nalias cd-a="composer dump-autoload"\nalias ci="composer install"\nalias cu="composer update"' >> ~/.zshrc
