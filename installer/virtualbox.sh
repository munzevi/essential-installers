#! /bin/bash
echo '#####################################################'
echo 'install virtualbox'
echo '#####################################################'
sudo apt-get install software-properties-common
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] http://download.virtualbox.org/virtualbox/debian $(lsb_release -cs) contrib"
sudo apt install virtualbox

