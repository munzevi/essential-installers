#! /bin/bash
echo '#####################################################'
echo 'install virtual Ulauncher'
echo '#####################################################'
sudo add-apt-repository ppa:agornostal/ulauncher
wget https://github.com/Ulauncher/Ulauncher/releases/download/5.8.1/ulauncher_5.8.1_all.deb
sudo apt install ./ulauncher_5.8.1_all.deb

