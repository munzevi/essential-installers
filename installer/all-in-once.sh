#! /bin/bash

echo 'updating system repositories'
sudo apt-get update -y
echo -e '\e[1;42;37m##################################################### \e[0m'
echo -e '\e[1;42;37m apt updated  \e[0m'
echo -e '\e[1;42;37m##################################################### \e[0m'


echo 'install snap, nano, curl, jdk, unzip and git'
sudo apt install curl snapd nano git default-jdk unzip -y
echo -e '\e[1;42;37m##################################################### \e[0m'
echo -e '\e[1;42;37m snap, curl, nano, git, unzip and jdk installed  \e[0m'
echo -e '\e[1;42;37m##################################################### \e[0m'

# Python3, Pip3, Virtualenv

echo 'install python3,pip3 and virtualenv'
sudo apt install software-properties-common build-essential cmake pkg-config libglib2.0-dev libgpgme-dev libgnutls28-dev uuid-dev libssh-gcrypt-dev libldap2-dev doxygen graphviz libradcli-dev libhiredis-dev libpcap-dev bison libksba-dev libsnmp-dev gcc-mingw-w64 heimdal-dev libpopt-dev xmltoman redis-server xsltproc libical-dev gnutls-bin nmap rpm nsis wget fakeroot gnupg sshpass socat snmp smbclient libmicrohttpd-dev libxml2-dev python3-polib gettext rsync xml-twig-tools python3-paramiko python3-lxml python3-defusedxml python3-pip python3-psutil python3-impacket virtualenv vim texlive-latex-extra texlive-fonts-recommended -y
echo -e '\e[1;42;37m##################################################### \e[0m'
echo -e '\e[1;42;37m python3, vim, wget, virtualenv and some depencies are installed \e[0m'
echo -e '\e[1;42;37m##################################################### \e[0m'

# Install latest JDK 

echo 'install php & extensions'
sudo add-apt-repository ppa:ondrej/php -y
sudo apt-get update
sudo apt install php8.0 php8.0-{bcmath,bz2,intl,gd,mbstring,mysql,zip,common,cli,fpm,mysql,curl,imagick} -y 	
echo -e '\e[1;42;37m##################################################### \e[0m'
echo -e '\e[1;42;37m apache webserver with local.dev virtual domain, composer and php8.0 installed with its extensions \e[0m'
echo -e '\e[1;42;37m##################################################### \e[0m'

# Node latest lts

echo 'install node and nvm'
sudo snap install node --classic
sudo snap install nvm-cli
echo -e '\e[1;42;37m##################################################### \e[0m'
echo -e '\e[1;42;37m npm and nvm installed \e[0m'
echo -e '\e[1;42;37m##################################################### \e[0m'


echo 'installing & starting mysql 8 service'
sudo apt install mysql-server -y 
sudo snap install mysql-shell 
sudo snap install dbeaver-ce
echo -e '\e[1;42;37m##################################################### \e[0m'
echo -e '\e[1;42;37m mysql-server and dbeaver installed \e[0m'
echo -e '\e[1;42;37m##################################################### \e[0m'

# Create new mysql user

echo 'creating mysqluser user without a password'
echo '#####################################################'
set -e

sudo mysql -uroot <<MYSQL_SCRIPT
CREATE USER 'mysqluser'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'mysqluser'@'localhost';
FLUSH PRIVILEGES;
MYSQL_SCRIPT
echo -e '\e[1;42;37m##################################################### \e[0m'
echo -e '\e[1;42;37m added sql user \e[0m'
echo -e '\e[1;42;37m##################################################### \e[0m'

# Insomnia

echo 'install insomnia and postman'
 
sudo snap install insomnia
sudo snap install postman
echo -e '\e[1;42;37m##################################################### \e[0m'
echo -e '\e[1;42;37m postman and insomia installed\e[0m'
echo -e '\e[1;42;37m##################################################### \e[0m'

# Virtualbox

echo 'install virtualbox'
'sudo apt-get install software-properties-common -y
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] http://download.virtualbox.org/virtualbox/debian $(lsb_release -cs) contrib"
sudo apt install virtualbox -y'
echo -e '\e[1;42;37m##################################################### \e[0m'
echo -e '\e[1;42;37m virtualbox installed\e[0m'
echo -e '\e[1;42;37m##################################################### \e[0m'

# GoogleChrome

echo 'install chrome and opera browsers'
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome-stable_current_amd64.deb
'sudo snap install opera'
echo -e '\e[1;42;37m##################################################### \e[0m'
echo -e '\e[1;42;37m chrome and opera installed\e[0m'
echo -e '\e[1;42;37m##################################################### \e[0m'

# GnomeTweak

echo 'install gnome tweak'
'sudo apt-add-repository universe
sudo apt install gnome-tweak-tool -y'
echo -e '\e[1;42;37m##################################################### \e[0m'
echo -e '\e[1;42;37m gnome-tweak-tool installed\e[0m'
echo -e '\e[1;42;37m##################################################### \e[0m'

# Virtual Studio install

echo 'install virtual studio'
sudo snap install --classic code
echo -e '\e[1;42;37m##################################################### \e[0m'
echo -e '\e[1;42;37m vs code installed\e[0m'
echo -e '\e[1;42;37m##################################################### \e[0m'

#docker

echo 'install docker'
'curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh
sh install.sh'
echo -e '\e[1;42;37m##################################################### \e[0m'
echo -e '\e[1;42;37m docker installed\e[0m'
echo -e '\e[1;42;37m##################################################### \e[0m'


# Ulauncher install
echo 'install virtual Ulauncher'
'sudo add-apt-repository ppa:agornostal/ulauncher -y
wget https://github.com/Ulauncher/Ulauncher/releases/download/5.8.1/ulauncher_5.8.1_all.deb
sudo apt install ./ulauncher_5.8.1_all.deb -y
rm -rf ./ulauncher_5.8.1_all.deb
'
echo -e '\e[1;42;37m##################################################### \e[0m'
echo -e '\e[1;42;37m docker installed\e[0m'
echo -e '\e[1;42;37m##################################################### \e[0m'

# Stacker install
echo 'install virtual sacker'
'sudo snap install stacker'
echo -e '\e[1;42;37m##################################################### \e[0m'
echo -e '\e[1;42;37m stacker installed\e[0m'
echo -e '\e[1;42;37m##################################################### \e[0m'


#zsh
echo 'install zsh'
sudo apt install zsh -y
echo '
alias ll="ls -lah" 
alias publicIp="curl ifconfig.me" 
alias please="sudo" 
alias ifcon="curl ifconfig.me" 

alias pa="php artisan" 
alias pas="php artisan serve"
alias pam="php artisan migrate"
alias pac="php artisan make:controller"
alias pat="php artisan tinker"
alias pam:m="php artisan make:migrate"
alias pad:s="php artisan db:seed"
alias pam:r="php artisan migrate:refresh"
alias par:l="php artisan route:list"

alias cd-a="composer dump-autoload"
alias ci="composer install"
alias cu="composer update"
' >> ~/.zshrc

#bash
echo 'aliases bash zsh'
echo '
alias ll="ls -lah" 
alias publicIp="curl ifconfig.me" 
alias please="sudo" 
alias ifcon="curl ifconfig.me" 

alias pa="php artisan" 
alias pas="php artisan serve"
alias pam="php artisan migrate"
alias pac="php artisan make:controller"
alias pat="php artisan tinker"
alias pam:m="php artisan make:migrate"
alias pad:s="php artisan db:seed"
alias pam:r="php artisan migrate:refresh"
alias par:l="php artisan route:list"

alias cd-a="composer dump-autoload"
alias ci="composer install"
alias cu="composer update"
' >> ~/.bashrc

echo 'install android studio sdk and emulator'
# Install latest JDK
sudo apt install default-jdk
# install unzip if not installed yet
sudo apt install unzip
# get latest sdk tools - link will change. go to https://developer.android.com/studio/#downloads to get the latest one
cd ~
wget https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip
# unpack archive
unzip sdk-tools-linux-4333796.zip
rm sdk-tools-linux-4333796.zip
mkdir android-sdk
mv tools android-sdk/tools
echo "export ANDROID_HOME=$HOME/android-sdk\nexport PATH=$PATH:$ANDROID_HOME/tools/bin\nexport PATH=$PATH:$ANDROID_HOME/platform-tools\nexport JAVA_OPTS='-XX:+IgnoreUnrecognizedVMOptions --add-modules java.se.ee'" >> ~./zshrc

source ~/.zshrc

sdkmanager "platform-tools" "platforms;android-28"

echo -e '\e[1;42;37m##################################################### \e[0m'
echo -e '\e[1;42;37m android studio sdk and emulator installed\e[0m'
echo -e '\e[1;42;37m##################################################### \e[0m'


echo 'install anaconda'
#conda
# depedenciess
apt-get install libgl1-mesa-glx libegl1-mesa libxrandr2 libxrandr2 libxss1 libxcursor1 libxcomposite1 libasound2 libxi6 libxtst6
# download
curl https://repo.anaconda.com/archive/Anaconda3-2021.05-Linux-x86_64.sh
# install
/bin/bash Anaconda3-2021.05-Linux-x86_64.sh
echo -e '\e[1;42;37m##################################################### \e[0m'
echo -e '\e[1;42;37m android studio sdk and emulator installed\e[0m'
echo -e '\e[1;42;37m##################################################### \e[0m'