#! /bin/bash
echo '#####################################################'
echo 'install imagemagick'
echo '#####################################################'
sudo apt-get install build-essential checkinstall && apt-get build-dep imagemagick -y
sudo wget http://www.imagemagick.org/download/ImageMagick.tar.gz
sudo tar xzvf ImageMagick.tar.gz
cd ImageMagick-7.0.4-5
./configure
make clean
make
checkinstall
ldconfig /usr/local/lib

