#! /bin/bash
echo '#####################################################'
echo 'installing & starting apache service'
echo '#####################################################'
sudo apt install apache2
sudo chown -R www-data:www-data /var/www
sudo a2enmod rewrite
sudo php7.4enmod mcrypt
sudo service apache2 restart

