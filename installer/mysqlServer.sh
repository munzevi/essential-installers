#! /bin/bash
echo '#####################################################'
echo 'installing & starting mysql 8 service'
echo '#####################################################'
sudo apt-get install wget zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev
wget https://dev.mysql.com/get/mysql-apt-config_0.8.15-1_all.deb
sudo dpkg -i mysql-apt-config_0.8.15-1_all.deb 
sudo apt update
sudo apt install mysql-server-8.0 mysql-client 

# Create new mysql user
echo '#####################################################'
echo 'creating new mysql user'
echo '#####################################################'
set -e

sudo mysql -uroot <<MYSQL_SCRIPT
CREATE USER 'phpmyadmin'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON phpmyadmin.* TO 'phpmyadmin'@'localhost';
FLUSH PRIVILEGES;
MYSQL_SCRIPT


